FROM python:3.6-alpine
LABEL name "master-flask"
LABEL maintainer "Hildeberto Abreu Magalhães <hildeberto@gmail.com>"

COPY ./app /app
WORKDIR /app
RUN pip install pipenv
RUN pipenv install

EXPOSE 5000
